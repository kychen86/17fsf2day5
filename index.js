// Load express
var express =  require("express");

var app = express();

app.get("/randomimg", function(req, resp){
    resp.status(200);
    resp.type("image/png");
    resp.sendFile(__dirname + "/picture/" + picId()+".png");        
});

//If using array
app.get("/imgrandom",function(req,res){
    res.status(200);
    res.sendFile(__dirname + "/picture/" + picray[picrayId()]);
});


app.use(express.static(__dirname + "/public"));

//Initalize port
var port = process.argv[2] || 3000;
//Listening for port
app.listen(port, function(){
    console.log("Application started at port %d", port);
})

//Randomize picture
function picId(){
    var randpic = Math.floor(Math.random()*5);// Math.random() return value between 0 and 1, Math.floor() return value rounded down.
    return randpic;
}

//If using an array
var picray =["butterfly.jpg","city.jpg","desert.jpg","flower.jpg","flowers.jpg"]

function picrayId(){
    var picrand = Math.floor(Math.random()*picray.length);
    return picrand;
}